package es.eug.informatica.cfgs.entorns.inicipoo.models;

/**
 * The Car class is just a sample to train some 
 * POO skills.
 * 
 * @author Xavi Sarda
 *
 */
public class Car {

	private String model;
	private String brand;
	private String color;
	private float price;
	
	/**
	 * Parametrized constructor
	 * 
	 * @param model Car model
	 * @param brand Car brand
	 * @param color Car color
	 * @param price Car price
	 */
	public Car(String model, String brand, String color, float price) {
		this.setModel(model);
		this.setBrand(brand);
		this.setColor(color);
		this.setPrice(price);
	}
	
	/**
	 * Getter method for the model attribute.
	 * 
	 * @return String with the car model
	 */
	public String getModel() {
		return model;
	}
	
	/**
	 * Setter method for the model attribute
	 * 
	 * @param model Car model
	 */
	public void setModel(String model) {
		this.model = model;
	}
	
	/**
	 * Getter method for the brand attribute.
	 * 
	 * @return String with the car brand
	 */
	public String getBrand() {
		return brand;
	}
	
	/**
	 * Setter method for the brand attribute
	 * 
	 * @param brand Car brand
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	/**
	 * Getter method for the color attribute.
	 * 
	 * @return String with the car color, not a Color instance
	 */
	public String getColor() {
		return color;
	}
	
	/**
	 * Setter method for the color attribute
	 * 
	 * @param color Car color
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Getter method for the price attribute.
	 * 
	 * @return float with the car price
	 */
	public float getPrice() {
		return price;
	}
	
	/**
	 * Setter method for the price attribute
	 * 
	 * @param price float value of the Car price
	 */
	public void setPrice(float price) {
		this.price = price;
	}
	
}
