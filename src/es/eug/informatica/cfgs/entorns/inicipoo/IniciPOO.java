package es.eug.informatica.cfgs.entorns.inicipoo;
import es.eug.informatica.cfgs.entorns.inicipoo.models.Car;

/**
 * IniciPOO is just a Main class for the project,
 * aiming to be a place to start learning some POO
 * fundamentals.
 * 
 * @author Xavi Sarda
 *
 */
public class IniciPOO {

	/**
	 * The execution will start with this method.
	 * 
	 * @param args Execution arguments
	 */
	public static void main(String[] args) {
		
		Car c = new Car("Megane", "Renault", "White", 15000);
		System.out.println(c.getBrand()+" "+c.getModel()+" "+c.getPrice());
		System.out.println("Hola");
		
		/*
		 * Equivalente a 
		 * 
		 * Car c = new Car();
		 * c.setBrand("Megane");
		 * c.setModel("Renault");
		 * c.setColor("White");
		 * c.setPrice(15000);
		 * System.out.println(c.getBrand()+" "+c.getModel()+" "+c.getPrice());
		 */
	}

}
